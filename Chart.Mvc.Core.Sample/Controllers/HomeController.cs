﻿using Microsoft.AspNetCore.Mvc;

namespace Chart.Mvc.Core.Sample.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ComplexCharts()
        {
            return View();
        }

        public ActionResult SimpleCharts()
        {
            return View();
        }

        public ActionResult QuickStart()
        {
            return View();
        }
    }
}

﻿using System;
using Chart.Mvc.Core.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Chart.Mvc.Core.Tests.Extensions
{
    [TestClass]
    public class ObjectExtensionsTests
    {
        [TestMethod]
        public void CanSerializeToJson()
        {
            var json = DateTime.Now.ToJson();
            Assert.IsFalse(string.IsNullOrWhiteSpace(json));
        }
    }
}

﻿using System.Collections.Generic;

namespace Chart.Mvc.Core.SimpleChart
{
    public class SimpleDataJson
    {
        public IList<SimpleDataset> Datasets { get; set; }
        public IEnumerable<string> Labels { get; set; }
    }
}

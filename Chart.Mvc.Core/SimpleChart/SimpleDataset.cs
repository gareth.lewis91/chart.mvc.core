﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chart.Mvc.Core.SimpleChart
{
    public class SimpleDataset
    {
        public string Label { get; set; }
        public IEnumerable<double> Data { get; set; }
        public IEnumerable<string> BackgroundColor { get; set; }
        public IEnumerable<string> HoverBackgroundColor { get; set; }
    }
}
